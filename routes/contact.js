var express = require('express');
var nodemailer = require('nodemailer');
var router = express.Router();

const transporter = nodemailer.createTransport({
     service: 'gmail',
     auth: {
          user: process.env.EMAIL_USERNAME,
          pass: process.env.EMAIL_PASSWORD
     }
});

var mailOptions = {
     to: 'coolbots7+coolbots7io@gmail.com'
};

/* GET home page. */
router.get('/', function(req, res, next) {
     res.render('contact', {
          title: 'Express'
     });
});

router.post('/submit_message', function(req, res, next) {
     console.table(req.body);

     mailOptions.from = req.body.email;
     mailOptions.subject = req.body.subject;
     mailOptions.html = "<html><head></head><body>";
     mailOptions.html += "From: " + req.body.email + "<br /><br />";
     mailOptions.html += "Subject: " + req.body.subject + "<br /><br />";
     mailOptions.html += "Message: <br />" + req.body.message;
     mailOptions.html += "</body></html>";

     transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
               console.log(error);
          } else {
               console.log('Email sent: ' + info.response);
          }
     });
     
     res.redirect('/contact');
});

module.exports = router;
